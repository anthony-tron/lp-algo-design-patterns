package examples.rpg;

public class ElfNinja implements Ninja {
    @Override
    public void hide() {
        System.out.println("Elf Ninja is hiding!");
    }
}
