package examples.rpg;

public class GoblinCharacterFactory implements CharacterFactory {

    @Override
    public Ninja createNinja() {
        return new GoblinNinja();
    }

    @Override
    public Warrior createWarrior() {
        return new GoblinWarrior();
    }

    @Override
    public Wizard createWizard() {
        return new GoblinWizard();
    }
}
