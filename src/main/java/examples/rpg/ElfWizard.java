package examples.rpg;

public class ElfWizard implements Wizard {
    @Override
    public void useMagic() {
        System.out.println("Elf Wizard is shooting a light ball!");
    }
}
