package examples.rpg;

public class HumanWizard implements Wizard {
    @Override
    public void useMagic() {
        System.out.println("Human Wizard is summoning a tornado!");
    }
}
