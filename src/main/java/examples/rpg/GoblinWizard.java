package examples.rpg;

public class GoblinWizard implements Wizard {
    @Override
    public void useMagic() {
        System.out.println("Goblin Wizard is shooting an ice beam!");
    }
}
