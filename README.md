# Design Patterns Demos

This is in an introduction to some design patterns.
There is some Java code to illustrate them as well as UML
diagrams.

## Abstract Factory pattern

Example is at `src/main/java/examples/rpg`.

# Running tests

Note: **you do not need to install `gradle`** to run the tests.

- Linux / Mac users: run `./gradlew test`
- Windows users: run `./gradlew.bat test`

